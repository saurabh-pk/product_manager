var ObjectID = require('mongodb').ObjectID;
module.exports = function () {
    this.isNull = function (val) {
        if ((val == [] || val == null || val == "" || val == undefined || typeof val == 'undefined') && typeof val != 'boolean' && typeof val != 'number') {
            return true;
        } else {
            if (typeof val == 'number' && isNaN(val)) {
                return true;
            }
            val = JSON.parse(JSON.stringify(val));
            return (typeof val == 'object') ? !Object.keys(val).length : false;
        }
    };
    this.getObjectId = function (id) {
        try {
            return new ObjectID(String(id));
        } catch (e) {
            if (e) {
                console.log("Error ", e, "for id ", id);
            }
        }
    }
    this.stringToObjectIdArray = function (task_arr) {
        if (typeof task_arr == 'string') {
            task_arr = task_arr.split(',');
        }
        var tmp_arr = [];
        for (var i = 0; i < task_arr.length; i++) {
            if (String(task_arr[i]).indexOf(",") > -1) {
                var split_ids = String(task_arr[i]).split(",");
                var tmp = stringToObjectIdArray(split_ids);
                tmp_arr = tmp_arr.concat(tmp);
            } else {
                console.log("task_arr[i]", task_arr[i]);
                tmp_arr.push(getObjectId(task_arr[i]));
            }
        }
        return tmp_arr;
    }
}