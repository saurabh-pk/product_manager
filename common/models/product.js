'use strict';
require('remoteMethodDisable')();
require('common_functions')();
module.exports = function (Product) {
    disableAll(Product);
};
